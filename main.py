from fastapi import FastAPI, Request
from fastapi.templating import Jinja2Templates

app = FastAPI()

templates = Jinja2Templates(directory='views')

CATS = [{'name': 'Maung', 'type': 'Domestic'}, {'name': 'Kunlun', 'type': 'Persian'}]
DOGS = [{'name': 'Bruno', 'type': 'Siberian'}, {'name': 'Cutie', 'type': 'Puddle'}]

@app.get('/cats')
async def name(request: Request):
  return templates.TemplateResponse('cats.html', {'request': request, 'name': 'Cat List', 'cats': CATS})

@app.get('/dogs')
async def name(request: Request):
  return templates.TemplateResponse('dogs.html', {'request': request, 'name': 'Dog List', 'dogs': DOGS})
