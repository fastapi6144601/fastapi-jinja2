# Do this after git clone (Run the program with the command):
- python -m venv venv
- venv\Scripts\activate.bat (windows) / source venv/bin/activate (Mac/Linux)
- pip install -r requirements.txt
- uvicorn main:app --port 3000  --reload
